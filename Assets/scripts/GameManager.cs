﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour{

    public static bool gameEnded;
    public GameObject GameOverUI;
    public Text roundsNumberTxt;
    public static int roundsNumber;

    void Start() {
        gameEnded = false;
        roundsNumber = 0;
    }

    void Update() {
        if (gameEnded)
            return;
        if (PlayerStats.Lives <= 0) {
            EndGame();
        }
        if (Input.GetKeyDown(KeyCode.E)) {
            EndGame();
        }
    }
    public void EndGame() {
        gameEnded = true;
        GameOverUI.SetActive(true);
        roundsNumberTxt.text = roundsNumber.ToString();
        PlayerStats.playing = false;
       // roundsNumber.text = PlayerStats;
    }
}
