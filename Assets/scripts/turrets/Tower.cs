﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    [Header("Enemigos")]
    public string enemyTag = "Enemy";
    private Transform target;
    private Enemy targetEnemy;

    [Header("General")]
    public BulletType bulletType;
    public float range = 6f;
    public GameObject bulletPref;
    public Transform firePoint;
    public float fireRate = 1f;
    public int Damage = 4;
    [HideInInspector]
    public float fireCountdown = 0f;
    public bool hasAnimation = true;

    [Header("Rotaciones")]
    public bool lookToEnemies = true;
    public bool doThisRotate = false;
    public Transform TowerRotate;
    public float rotSpeed = 15f;



	void Start () {
        //bulletType.GetComponent<BulletType>();
        //Hacer que se carguen los tipos de bala
        bulletType.TipoDeBala();
        //llama a la funcion UpdateTarget 1 vez cada 0.25 segundos
        InvokeRepeating("UpdateTarget", 0f, 0.1f);

        //coge todos los clips de animacion de la torreta para leer la velocidad de disparo
        if (hasAnimation){
            Animator Anim = GetComponent<Animator>();
            AnimationClip[] clips = Anim.runtimeAnimatorController.animationClips;
            foreach(AnimationClip clip in clips) {
                if(clip.name == "Attack") {
                    fireRate = clip.length;
                }
            }
        }
	}

    //Para encontrar enemigos cercanos
    void UpdateTarget () {
        //coge en un array todos los GameObjects con una tag "enemyTag"
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        //calcular enemigo más cercano (nearestEnemy)
        //a cada enemigo calcula su distancia y si ese tiene menos que todos los demás lo añade a nearestEnemy
        foreach (GameObject enemy in enemies) {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance) {

                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        //Asignar el enemigo más cercano a su variable
        //si no hay un nearestEnemy y hay un enemigo cercano a rango apunta hacia ese enemigo
        if(nearestEnemy != null && shortestDistance <= range) {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
            if(lookToEnemies){
                if (target.position.x < transform.position.x) {
                    this.transform.rotation = new Quaternion(this.transform.rotation.x,180, this.transform.rotation.z,0);
                } else {
                    this.transform.rotation = new Quaternion(this.transform.rotation.x, 0, this.transform.rotation.z,0);
                }
            }
        } else {
            target = null;
        }
	}
    //disparar y recargar
    void Update() {
        if (target == null)
            return;
        if (!GameManager.gameEnded) {
            //cada frame mira si puede hacer LockOnTarget y si ha disparado hace el cooldown, tambien si ha disparado para la animacion de disparo
            LockOnTarget();
            if (fireCountdown <= 0f) {
                Shoot();
                fireCountdown = 1f / fireRate;
            } else {
                if (hasAnimation) {
                    gameObject.GetComponent<Animator>().SetBool("Shoot", false);
                }
                fireCountdown -= Time.deltaTime;
            }
        }
    }

    //Rotar hacia un target
    void LockOnTarget() {
        //si esta torreta es 3d y puede rotar lo hace, si no no hace nada
        if (doThisRotate) {
            Vector3 dir = target.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(TowerRotate.rotation, lookRotation, Time.deltaTime * rotSpeed).eulerAngles;
            TowerRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        } else {
            TowerRotate = null;
        }

    }

    //Disparar al objetivo
    void Shoot() {
        //crear la bala y ponerle su objetivo
        Debug.Log("DISPARO: " + target);

        GameObject bulletGO = (GameObject)Instantiate(bulletPref, firePoint.position, new Quaternion(0,0,0,0));
        if(bulletType.bulletType == "Bala"){
            Bullet bullet = bulletGO.GetComponent<Bullet>();
            //le envia quien es el objetivo a la bala y la torreta hace la animacion de disparar
            if(bullet != null) {
                bullet.Seek(target);
                bullet.damage = Damage;
                if(hasAnimation){
                    gameObject.GetComponent<Animator>().SetBool("Shoot", true);
                }
            }

        }else if(bulletType.bulletType == "Cohete"){
            bullet_homing bullet = bulletGO.GetComponent<bullet_homing>();
            //le envia quien es el objetivo a la bala y la torreta hace la animacion de disparar
            if(bullet != null) {
                bullet.Seek(target);
                bullet.damage = Damage;
                if (hasAnimation){
                    gameObject.GetComponent<Animator>().SetBool("Shoot", true);
                }
            }
        }
    }

    //crear visualmente el rango
    void OnDrawGizmos() {

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
