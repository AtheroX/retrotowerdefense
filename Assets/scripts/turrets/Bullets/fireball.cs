﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireball : MonoBehaviour {

    // Este script controla el giro y crecimiento de las bolas de fuego

    public float maxf;
    public float minf;
    Vector3 min;
    Vector3 max;
    public float time = 0.3f;
    float t;
    public float rotvel;

    void Start () {
        maxf = 0.8f;
        minf = 0.3f;
        min = new Vector3(minf, minf, minf);
        max = new Vector3(maxf, maxf, maxf);
    }
	
	void Update () {
        t += Time.deltaTime;
        transform.localScale = Vector3.Lerp(min, max, t/time);

        transform.Rotate(new Vector3(0, 0, rotvel*Time.deltaTime));
	}
}
