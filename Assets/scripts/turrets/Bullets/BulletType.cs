﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// este script genera un menu desplegable dentro del script Bullet con los tipos de balas
[System.Serializable]
public class BulletType{

    public Type type;
    [HideInInspector]
    public string bulletType;

    public void TipoDeBala(){
        if(type == Type.Bala){
            bulletType = "Bala";
        }else if(type == Type.Cohete){
            bulletType = "Cohete";
        }
    }
}
public enum Type{
    Bala,
    Cohete,
    LaserTarget,
    Laser4Dir,
    Creep
}
