﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour {

    [Header("Colores")]
    //color al tener raton encima
    public Color hoverColor;
    //Color de no tener suficiente dinero
    public Color NotMoneyColor;

    public float metalicHover = 0.6f;
    public float metalicNotMoney = 0.6f;

	//la torreta colocada
    [Header("Datos")]
    public GameObject turret;

    private MeshRenderer rend;
    private Color startColor;

    //BuildManager es el singleton para construir
    BuildManager buildManager;

    private void Start() {
		//obtener el componente de renderizar colores del nodo y obtener su color inicial
        rend = GetComponent<MeshRenderer>();
        startColor = rend.material.color;

        //se le asigna para escribir más rápido
        buildManager = BuildManager.instance;
    }

    public Vector3 GetBuildPosition() {
        return transform.position;
    }


    private void OnMouseDown() {

        //si de inicio no has clicado ninguna torreta, que no de error
        if (!buildManager.CanBuild)
            return;

        //mira si tiene una torreta colocada, si no, no puedes construir
        if (turret != null) {
            Debug.Log("Ocupado!");
			//Aquí hay que hacer que al clicar un sitio que ya tiene torreta, poderla vender.
            return;
        }

        buildManager.BuildTurretHere(this);

    }


    void Update() {
        
        //Si la funcion canbuild da negativo no continua el codigo
        if (!buildManager.CanBuild)
            return;

        //Si tiene dinero que pase X
        if (buildManager.HasMoney) {
            //con el raton dentro cambia de color
            rend.material.color = hoverColor;
            rend.material.SetFloat("_Metallic", metalicHover);
        } else {
            rend.material.color = NotMoneyColor;
            rend.material.SetFloat("_Metallic", metalicNotMoney);
        }

    }

    void OnMouseExit() {

        //al sacar el raton vuelve al original
        rend.material.color = startColor;
        rend.material.SetFloat("_Metallic", 0f);

    }
}
