﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour {

    public Transform enemyPref;
    public Transform spawnPoint;

    public float timeBetweenWaves = 10f;
    private float countdown = 7f;

    public Text waveCountdownText;
    private int waveIndex = 1;

    void Update() {
        if (!GameManager.gameEnded) {
            //Si el contador llega a 0 (está fijado como minimo, asi que si o si va a llegar), nueva ronda.
            if (countdown == 0) {
                StartCoroutine(SpawnWave());
                countdown = timeBetweenWaves;
            }
            countdown -= Time.deltaTime;

            //Fijar un minimo en el contador
            countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);

            //Asignar al texto el contador
            waveCountdownText.text = string.Format("{0:00}", countdown);
        }
    }

    //spawnear malotes en el spawnpoint segun la cantidad de ronda
    IEnumerator SpawnWave() {
        if (!GameManager.gameEnded) {
            for (int i = 0; i < waveIndex; i++) {
                SpawnEnemy();
                yield return new WaitForSeconds(0.5f);
            }
            waveIndex++;
            GameManager.roundsNumber++;
        }
    }

    void SpawnEnemy() {
        Instantiate(enemyPref, spawnPoint.position, spawnPoint.rotation);
    }
}
