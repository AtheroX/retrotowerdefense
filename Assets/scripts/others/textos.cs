﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textos : MonoBehaviour
{
    public Text moneyTxt;
    public Text txtFPS;
    public Text txtVersion;

    // Update is called once per frame
    void Start(){
        txtVersion.text = "v" + Application.version;
        InvokeRepeating("updateframes", 0f, 1.5f);
    }

    void FixedUpdate() {
        moneyTxt.text = "$" + PlayerStats.Money.ToString();
    }

    void updateframes(){
        float current = 0;
        current = (int)(1 / Time.unscaledDeltaTime);
        txtFPS.text = current.ToString() + " FPS";
    }
}
